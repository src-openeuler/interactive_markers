# interactive_markers

#### 介绍
interactive_markers用于RViz和类似工具的3D交互式标记通信库.交互式标记类似于先前教程中描述的“常规”标记，但是它们允许用户通过更改其位置或旋转，单击它们或从分配给每个标记的上下文菜单中选择某些内容来与它们进行交互。

#### 软件架构
软件架构说明

![输入图片说明](https://images.gitee.com/uploads/images/2020/1114/214745_da70854a_1226697.png "interactive_marker_structure.png")

文件内容:
```
interactive_markers/
├── CHANGELOG.rst
├── CMakeLists.txt
├── include
│   └── interactive_markers
├── package.xml
├── setup.py
├── src
│   ├── interactive_marker_client.cpp
│   ├── interactive_markers
│   ├── interactive_marker_server.cpp
│   ├── menu_handler.cpp
│   ├── message_context.cpp
│   ├── single_client.cpp
│   ├── test
│   └── tools.cpp
└── test
    ├── cpp_client.test
    ├── cpp_server_client.test
    └── cpp_server.test

```

#### 安装教程

1. 下载rpm包

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-catkin/ros-noetic-ros-interactive_markers-1.12.0-1.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-catkin/ros-noetic-ros-interactive_markers-1.12.0-1.oe2203.x86_64.rpm  

2. 安装rpm包

aarch64:

sudo rpm -ivh ros-noetic-ros-interactive_markers-1.12.0-1.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-interactive_markers-1.12.0-1.oe2203.x86_64.rpm --nodeps --force

#### 使用说明

依赖环境安装:

sh /opt/ros/noetic/install_dependence.sh

安装完成以后，在/opt/ros/noetic/目录下有如下输出,则表示安装成功

输出:
```
interactive_markers/
├── cmake.lock
├── env.sh
├── lib
│   ├── interactive_markers
│   ├── libinteractive_markers.so
│   ├── pkgconfig
│   └── python2.7
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    └── interactive_markers
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
