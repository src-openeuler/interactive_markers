# interactive_markers

#### Description
interactive_markers 3D interactive marker communication library for RViz and similar tools.Interactive markers are similar to the "regular" markers described in the previous tutorials, however they allow the user to interact with them by changing their position or rotation, clicking on them or selecting something from a context menu assigned to each marker.

#### Software Architecture
Software architecture description

![输入图片说明](https://images.gitee.com/uploads/images/2020/1114/214610_c6bd83a5_1226697.png "interactive_marker_structure.png")

input:
```
interactive_markers/
├── CHANGELOG.rst
├── CMakeLists.txt
├── include
│   └── interactive_markers
├── package.xml
├── setup.py
├── src
│   ├── interactive_marker_client.cpp
│   ├── interactive_markers
│   ├── interactive_marker_server.cpp
│   ├── menu_handler.cpp
│   ├── message_context.cpp
│   ├── single_client.cpp
│   ├── test
│   └── tools.cpp
└── test
    ├── cpp_client.test
    ├── cpp_server_client.test
    └── cpp_server.test

```

#### Installation

1.  Download RPM

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-catkin/ros-noetic-ros-interactive_markers-1.12.0-1.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-catkin/ros-noetic-ros-interactive_markers-1.12.0-1.oe2203.x86_64.rpm 

2.  Install RPM

aarch64:

sudo rpm -ivh ros-noetic-ros-interactive_markers-1.12.0-1.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-interactive_markers-1.12.0-1.oe2203.x86_64.rpm --nodeps --force

#### Instructions

Dependence installation

sh /opt/ros/noetic/install_dependence.sh

Exit the following output file under the /opt/ros/noetic/ directory , Prove that the software installation is successful

output:
```
interactive_markers/
├── cmake.lock
├── env.sh
├── lib
│   ├── interactive_markers
│   ├── libinteractive_markers.so
│   ├── pkgconfig
│   └── python2.7
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    └── interactive_markers
```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
